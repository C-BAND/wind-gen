# wind-gen
提取[ruoyi](https://gitee.com/zhangmrit/RuoYi)代码生成模块，增加生成到项目选项和可视化配置

vm模版是自己使用的，比较简便，可以从原项目中获取并覆盖，亦可以修改源码，[模板地址](https://gitee.com/zhangmrit/RuoYi/tree/master/ruoyi-generator/src/main/resources/vm)

## 增加合并md生成api文档功能
- 参看`com.zmr.wind.gen.util.MdUtil`运行main方法会得到一个文件夹，打开html即可观看效果

##### 演示效果
<table>
    <tr>
        <td><img src="http://upload.ouliu.net/i/2019071118334876opf.gif"/></td>
    </tr>
</table>


## 欢迎pr,star或者打赏
##  请作者喝杯咖啡或者建设演示服务器

###### 微信 or 支付宝
<img src="https://gitee.com/zhangmrit/img/raw/master/contribute/wechat.png"/>
<img src="https://gitee.com/zhangmrit/img/raw/master/contribute/alipay.png"/>

###### 效果如下
<table>
    <tr>
        <td><img src="https://gitee.com/zhangmrit/wind-gen/raw/master/img/show.png"/></td>
    </tr>
</table>